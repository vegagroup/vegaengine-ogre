//---------------------------
// Projective texture section
//---------------------------


vertex_program Common/TexProjectionVP cg
{
	source Projection.cg
	entry_point generalPurposeProjection_vp
	profiles vs_1_1 arbvp1
}

fragment_program Common/TexProjectionFP cg
{
	source Projection.cg
	entry_point generalPurposeProjection_fp
	// sorry, ps_1_1 can't do this, fp20 can though
	profiles ps_2_0 arbfp1 fp20
}

material Common/GeneralTexProjection
{
	technique
	{
		pass 
		{
			
			vertex_program_ref Common/TexProjectionVP
			{
				param_named_auto worldViewProjMatrix worldviewproj_matrix
				param_named_auto worldMatrix world_matrix
				// You'll need to update the tex projection, I suggest using
				// the Frustum class
				//param_named_auto texWorldViewProj worldviewproj_matrix
			}
			fragment_program_ref Common/TexProjectionFP
			{
				// no params
			}
			texture_unit
			{
				// Project the OGRE logo
				texture ogrelogo.png
				tex_address_mode clamp
			}
		}
		
			
	}
    
}



vertex_program Common/TextureArrayVScg cg
{
	source TextureArrayVS.cg
	entry_point textureArray_v
	profiles vs_4_0

	default_params
	{
		param_named_auto worldViewProj worldviewproj_matrix
	}
}

vertex_program Common/TextureArrayVSglsl glsl
{
	source TextureArrayVS.glsl

	default_params
	{
		param_named_auto worldViewProj worldviewproj_matrix
	}
}

fragment_program Common/TextureArrayPSglsl glsl
{
	source TextureArrayPS.glsl

	default_params
	{
		param_named TextureArrayTex int 0
	}
}

fragment_program Common/TextureArrayPShlsl4 hlsl
{
	entry_point textureArray_p
	source TextureArrayPS.hlsl4
	target ps_4_0
}

fragment_program Common/TextureArrayPSasm asm
{
	source TextureArrayPS.asm
	syntax gp4fp
}

material Common/TextureArray
{
	technique
	{
		pass
		{
			fragment_program_ref Common/TextureArrayPSasm
			{
			}

		}
	}


	technique HLSL4
	{
		pass
		{
			vertex_program_ref Common/TextureArrayVScg
			{
			}

			fragment_program_ref Common/TextureArrayPShlsl4
			{
			}
		}
	}

	technique GLSL
	{
		pass
		{
			vertex_program_ref Common/TextureArrayVSglsl
			{
			}

			fragment_program_ref Common/TextureArrayPSglsl
			{
			}

			texture_unit
			{
				// Will be filled in at runtime
				texture TextureArrayTex
				tex_address_mode clamp
			}
		}
	}

}
