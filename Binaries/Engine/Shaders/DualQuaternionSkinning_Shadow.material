///////////////////////////////////////////////////////////////////////////
/// Declare shaders for casters
///////////////////////////////////////////////////////////////////////////

vertex_program RTShader/shadow_caster_dq_skinning_1weight_vs unified
{	
	delegate RTShader/shadow_caster_dq_skinning_1weight_vs_glsl
	delegate RTShader/shadow_caster_dq_skinning_1weight_vs_cg
}

vertex_program RTShader/shadow_caster_dq_skinning_2weight_vs unified
{
	delegate RTShader/shadow_caster_dq_skinning_2weight_vs_glsl
	delegate RTShader/shadow_caster_dq_skinning_2weight_vs_cg
}

vertex_program RTShader/shadow_caster_dq_skinning_3weight_vs unified
{	
	delegate RTShader/shadow_caster_dq_skinning_3weight_vs_glsl
	delegate RTShader/shadow_caster_dq_skinning_3weight_vs_cg
}

vertex_program RTShader/shadow_caster_dq_skinning_4weight_vs unified
{
	delegate RTShader/shadow_caster_dq_skinning_4weight_vs_glsl
	delegate RTShader/shadow_caster_dq_skinning_4weight_vs_cg
}
vertex_program RTShader/shadow_caster_dq_skinning_1weight_twophase_vs unified
{	
	delegate RTShader/shadow_caster_dq_skinning_1weight_twophase_vs_cg	
}

vertex_program RTShader/shadow_caster_dq_skinning_2weight_twophase_vs unified
{
	delegate RTShader/shadow_caster_dq_skinning_2weight_twophase_vs_cg
	delegate RTShader/shadow_caster_dq_skinning_2weight_twophase_vs_glsl
}

vertex_program RTShader/shadow_caster_dq_skinning_3weight_twophase_vs unified
{	
	delegate RTShader/shadow_caster_dq_skinning_3weight_twophase_vs_cg
	delegate RTShader/shadow_caster_dq_skinning_3weight_twophase_vs_glsl
}

vertex_program RTShader/shadow_caster_dq_skinning_4weight_twophase_vs unified
{	
	delegate RTShader/shadow_caster_dq_skinning_4weight_twophase_vs_cg
	delegate RTShader/shadow_caster_dq_skinning_4weight_twophase_vs_glsl
}

fragment_program RTShader/shadow_caster_dq_ps unified
{
	delegate RTShader/shadow_caster_dq_ps_glsl
	delegate RTShader/shadow_caster_dq_ps_cg
}

// declare the fragment shader (GLSL for the language)
fragment_program RTShader/shadow_caster_dq_ps_glsl glsl
{
	source DualQuaternionSkinning_ShadowPassThrough_ps.glsl
}

// declare the fragment shader (CG for the language)
fragment_program RTShader/shadow_caster_dq_ps_cg cg
{
	// source file
	source DualQuaternionSkinning_Shadow.cg
	// will run on pixel shader 2.0+
	profiles ps_2_0 gp4fp
	// entry function
	entry_point shadow_caster_dq_ps

 	default_params
	{
		
	}
}

vertex_program RTShader/DualQuaternionHardwareSkinningTwoWeightsCommon glsl
{
	source DualQuaternion_Common.glsl
}

vertex_program RTShader/shadow_caster_dq_skinning_1weight_vs_glsl glsl
{
	source DualQuaternionSkinning_ShadowOneWeight.glsl
	attach RTShader/DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_2weight_vs_glsl glsl
{
	source DualQuaternionSkinning_ShadowTwoWeights.glsl
	attach RTShader/DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_3weight_vs_glsl glsl
{
	source DualQuaternionSkinning_ShadowThreeWeights.glsl
	attach RTShader/DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_4weight_vs_glsl glsl
{
	source DualQuaternionSkinning_ShadowFourWeights.glsl
	attach RTShader/DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_1weight_vs_cg cg
{
	source DualQuaternionSkinning_Shadow.cg
	//will run on 3 to accommodate the amount of registers
	profiles vs_3_0 vp40 gp4vp
	entry_point shadow_caster_dq_skinning_1weight_vs

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_2weight_vs_cg cg
{
	source DualQuaternionSkinning_Shadow.cg
	//will run on 3 to accommodate the amount of registers
	profiles vs_3_0 vp40 gp4vp
	entry_point shadow_caster_dq_skinning_2weight_vs
	includes_skeletal_animation true

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_3weight_vs_cg cg
{
	source DualQuaternionSkinning_Shadow.cg
	//will run on 3 to accommodate the amount of registers
	profiles vs_3_0 vp40 gp4vp
	entry_point shadow_caster_dq_skinning_3weight_vs

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}


vertex_program RTShader/shadow_caster_dq_skinning_4weight_vs_cg cg
{
	source DualQuaternionSkinning_Shadow.cg
	//will run on 3 to accommodate the amount of registers
	profiles vs_3_0 vp40 gp4vp
	entry_point shadow_caster_dq_skinning_4weight_vs

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_1weight_twophase_vs_cg cg
{
	source DualQuaternionSkinning_Shadow.cg
	//will run on 3 to accommodate the amount of registers
	profiles vs_3_0 vp40 gp4vp
	entry_point shadow_caster_dq_skinning_1weight_twophase_vs

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto scaleM world_scale_shear_matrix_array_3x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_2weight_twophase_vs_cg cg
{
	source DualQuaternionSkinning_Shadow.cg
	//will run on 3 to accommodate the amount of registers
	profiles vs_3_0 vp40 gp4vp
	entry_point shadow_caster_dq_skinning_2weight_twophase_vs
	includes_skeletal_animation true

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto scaleM world_scale_shear_matrix_array_3x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program RTShader/shadow_caster_dq_skinning_2weight_twophase_vs_glsl glsl
{
	source DualQuaternionSkinning_ShadowCasterTwoPhaseTwoWeights.glsl
	//will run on 3 to accommodate the amount of registers
	attach RTShader/DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true

	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto scaleM world_scale_shear_matrix_array_3x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}
///////////////////////////////////////////////////////////////////////////
/// Declare materials for casters and casters with hardware skinning
///////////////////////////////////////////////////////////////////////////

material RTShader/shadow_caster_dq_skinning_1weight
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_1weight_vs
			{				
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}


material RTShader/shadow_caster_dq_skinning_2weight
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_2weight_vs
			{				
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}


material RTShader/shadow_caster_dq_skinning_3weight
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_3weight_vs
			{				
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}


material RTShader/shadow_caster_dq_skinning_4weight
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_4weight_vs
			{				
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}

material RTShader/shadow_caster_dq_skinning_1weight_twophase
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_1weight_twophase_vs
			{
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}


material RTShader/shadow_caster_dq_skinning_2weight_twophase
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_2weight_twophase_vs
			{				
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}


material RTShader/shadow_caster_dq_skinning_3weight_twophase
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_2weight_twophase_vs
			{				
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}


material RTShader/shadow_caster_dq_skinning_4weight_twophase
{
	technique
	{
		// all this will do is write depth and depth� to red and green
		pass
		{
			vertex_program_ref RTShader/shadow_caster_dq_skinning_2weight_twophase_vs
			{				
			}

			fragment_program_ref RTShader/shadow_caster_dq_ps
			{				
			}
		}
	}
}
