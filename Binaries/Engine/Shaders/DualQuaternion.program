///////////////////////////////////////////////////////////////////////////
/// Declare CG shaders for skinning
///////////////////////////////////////////////////////////////////////////
// Dual quaternion hardware skinning using two indexed weights per vertex
vertex_program DualQuaternionHardwareSkinningTwoWeightsCg cg
{
	source DualQuaternion.cg
	entry_point dualQuaternionHardwareSkinningTwoWeights_vp
    profiles vp40 vs_4_0 vs_3_0 vs_2_0 vs_1_1 arbvp1
	includes_skeletal_animation true
	
	default_params
	{
	    param_named_auto diffuse surface_diffuse_colour
	}
}

// Dual quaternion two phase hardware skinning using two indexed weights per vertex, supports scaling and shearing
vertex_program DualQuaternionHardwareSkinningTwoWeightsTwoPhaseCg cg
{
	source DualQuaternion.cg
	entry_point dualQuaternionHardwareSkinningTwoWeightsTwoPhase_vp
    profiles vp40 vs_4_0 vs_3_0 vs_2_0 vs_1_1 arbvp1
	includes_skeletal_animation true

	default_params
	{
	    param_named_auto diffuse surface_diffuse_colour
	}
}

///////////////////////////////////////////////////////////////////////////
/// Declare CG shaders for shadow casters
///////////////////////////////////////////////////////////////////////////
vertex_program DualQuaternionHardwareSkinningTwoWeightsShadowCasterCg cg
{
	source DualQuaternion.cg
	entry_point dualQuaternionHardwareSkinningTwoWeightsCaster_vp
    profiles vp40 vs_4_0 vs_3_0 vs_2_0 vs_1_1 arbvp1
	includes_skeletal_animation true
}

vertex_program DualQuaternionHardwareSkinningTwoWeightsTwoPhaseShadowCasterCg cg
{
	source DualQuaternion.cg
	entry_point dualQuaternionHardwareSkinningTwoWeightsTwoPhaseCaster_vp
	profiles vp40 vs_4_0 vs_3_0 vs_2_0 vs_1_1 arbvp1
	includes_skeletal_animation true
}

///////////////////////////////////////////////////////////////////////////
/// Declare GLSL shaders for skinning
///////////////////////////////////////////////////////////////////////////
vertex_program DualQuaternionHardwareSkinningTwoWeightsCommon glsl
{
	source DualQuaternion_Common.glsl
    syntax glsl
}

vertex_program DualQuaternionHardwareSkinningTwoWeightsGLSL glsl
{
	source DualQuaternion.glsl
    syntax glsl
	attach DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true
}

// Dual quaternion two phase hardware skinning using two indexed weights per vertex, supports scaling and shearing
vertex_program DualQuaternionHardwareSkinningTwoWeightsTwoPhaseGLSL glsl
{
	source DualQuaternion_TwoPhase.glsl
    syntax glsl
	attach DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true
}

///////////////////////////////////////////////////////////////////////////
/// Declare GLSL shaders for shadow casters
///////////////////////////////////////////////////////////////////////////
vertex_program DualQuaternionHardwareSkinningTwoWeightsShadowCasterGLSL glsl
{
	source DualQuaternion_ShadowCaster.glsl
    syntax glsl
	attach DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true
}

vertex_program DualQuaternionHardwareSkinningTwoWeightsTwoPhaseShadowCasterGLSL glsl
{
	source DualQuaternion_TwoPhaseShadowCaster.glsl
    syntax glsl
	attach DualQuaternionHardwareSkinningTwoWeightsCommon
	includes_skeletal_animation true
}

///////////////////////////////////////////////////////////////////////////
/// Declare unified shaders 
///////////////////////////////////////////////////////////////////////////
vertex_program DualQuaternionHardwareSkinningTwoWeights unified
{	
	delegate DualQuaternionHardwareSkinningTwoWeightsGLSL
	delegate DualQuaternionHardwareSkinningTwoWeightsCg
	
	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto lightPos[0] light_position 0
		param_named_auto lightPos[1] light_position 1
		param_named_auto lightDiffuseColour[0] light_diffuse_colour 0
		param_named_auto lightDiffuseColour[1] light_diffuse_colour 1
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program DualQuaternionHardwareSkinningTwoWeightsShadowCaster unified
{	
	delegate DualQuaternionHardwareSkinningTwoWeightsShadowCasterGLSL
	delegate DualQuaternionHardwareSkinningTwoWeightsShadowCasterCg
	
	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program DualQuaternionHardwareSkinningTwoWeightsTwoPhase unified
{
	delegate DualQuaternionHardwareSkinningTwoWeightsTwoPhaseGLSL
	delegate DualQuaternionHardwareSkinningTwoWeightsTwoPhaseCg
	
	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto scaleM world_scale_shear_matrix_array_3x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto lightPos[0] light_position 0
		param_named_auto lightPos[1] light_position 1
		param_named_auto lightDiffuseColour[0] light_diffuse_colour 0
		param_named_auto lightDiffuseColour[1] light_diffuse_colour 1
		param_named_auto ambient ambient_light_colour
	}
}

vertex_program DualQuaternionHardwareSkinningTwoWeightsTwoPhaseShadowCaster unified
{
	delegate DualQuaternionHardwareSkinningTwoWeightsTwoPhaseShadowCasterGLSL
	delegate DualQuaternionHardwareSkinningTwoWeightsTwoPhaseShadowCasterCg
	
	default_params
	{
		param_named_auto worldDualQuaternion2x4Array world_dualquaternion_array_2x4
		param_named_auto scaleM world_scale_shear_matrix_array_3x4
		param_named_auto viewProjectionMatrix viewproj_matrix
		param_named_auto ambient ambient_light_colour
	}
}
