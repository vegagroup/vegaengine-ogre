#pragma once

const Ogre::ColourValue SECTOR_COLOR(0.2f, 0.6f, 0.2f, 0.3f);
const Ogre::ColourValue AI_NAVI_COLOR(0.0f, 0.0f, 0.3f, 0.3f);
const Ogre::ColourValue PFX_COLOR(0.3f, 0.0f, 0.0f, 0.5f);
const Ogre::ColourValue LIGHT_COLOR(1.0f, 1.0f, 0.0f, 0.5f);