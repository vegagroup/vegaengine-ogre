/* VG CONFIDENTIAL
* VegaEngine(TM) Package 0.5.5.0
* Copyright (C) 2009-2014 Vega Group Ltd.
* Author: Nick Galko
* E-mail: nick.galko@vegaengine.com
* All Rights Reserved.
*/
#pragma once

#include "RenderPrivate.h"

#include "src/PostEffects/PostEffects.cpp"
#include "src/Externals.cpp"
#include "src/Render.cpp"
#include "src/RenderPrivate.cpp"
#include "src/RenderScripting.cpp"
#include "src/TerrainMaterialGeneratorD.cpp"
#include "src/VideoPlayer.cpp"
#include "src/ffmper_time.cpp"