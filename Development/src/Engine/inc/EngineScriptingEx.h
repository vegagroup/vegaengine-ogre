/* VG CONFIDENTIAL
* VegaEngine(TM) Package 0.5.5.0
* Copyright (C) 2009-2014 Vega Group Ltd.
* Author: Nick Galko
* E-mail: nick.galko@vegaengine.com
* All Rights Reserved.
*/
#pragma once

#include "Scripting.h"

namespace vega
{
	//Render.dll
	void _RenderAddScriptFunctionaluty(Scripting* mScript);
	//Engine.dll
	void _EngineAddScriptFunctionaluty(Scripting* mScript);
} 