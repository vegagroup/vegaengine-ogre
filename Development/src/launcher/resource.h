//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by launcher.rc
//
#define VS_VERSION_INFO                 1
#define IDI_ICON                        101
#define IDC_CRYSISCURSOR_AMBER          103
#define IDC_CRYSISCURSOR_BLUE           104
#define IDC_CRYSISCURSOR_GREEN          105
#define IDC_CRYSISCURSOR_RED            106
#define IDC_CRYSISCURSOR_WHITE          107

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
