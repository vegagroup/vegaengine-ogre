/* VG CONFIDENTIAL
* VegaEngine(TM) Package 0.5.5.0
* Copyright (C) 2009-2014 Vega Group Ltd.
* Author: Nick Galko
* E-mail: nick.galko@vegaengine.com
* All Rights Reserved.
*/
#pragma once

//-----------------------------------------------------------------------------
// Standard includes
//-----------------------------------------------------------------------------

// Engine Interfaces
#include "../../Common/inc/IEngine.h"

#include "buildconfig.h"
#include "Core.h"
#include "DynLib.h"
#include "log.h"
#include "Shared.h"
#include "Config.h"
#include "FileSystem.h"
#include "EngineConfig.h"
#include "CoreSystems.h"
#include "VFile.h"
#include "VMathTranslations.h"
#include "Scripting.h"