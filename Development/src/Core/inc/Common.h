#pragma once

namespace vega
{
	// computing build id
	extern const char* build_date;
	extern int build_id;
}