#pragma once

#include "CorePrivate.h"


#include "src\stdafx.cpp"
#include "src\Additions.cpp"
#include "src\Common.cpp"
#include "src\Config.cpp"
#include "src\CoreSystems.cpp"
#include "src\dynlib.cpp"
#include "src\ErrorDialog.cpp"
#include "src\FileSystem.cpp"
#include "src\FileSystemLayerImpl_Android.cpp"
#include "src\FileSystemLayerImpl_OSX.cpp"
#include "src\FileSystemLayerImpl_WIN32.cpp"
#include "src\log.cpp"
#include "src\OgreStaticPluginLoader.cpp"
#include "src\Scripting.cpp"
#include "src\steamworks.cpp"
#include "src\Str.cpp"
#include "src\VFile.cpp"